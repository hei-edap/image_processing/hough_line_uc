#include <stdlib.h>
#include <math.h>
#include "sod.h"

int main(int argc, char *argv[]) {
	const char *zInput = argv[1];
	sod_img imgIn = sod_img_load_grayscale(zInput);
	sod_img cannyImg = sod_canny_edge_image(imgIn, 0);
	sod_free_image(cannyImg);
	return 0;
}

