Image number,Angle min,Angle max,Gauge min,Gauge max,Unit,Current value,Type
1,44,302,0,350,bar,0,inter1
2,43,309,0,350,bar,113,inter1
3,44,304,0,350,bar,0,inter2
4,29,336,0,350,bar,218,msa
5,42,309,0,350,bar,69,inter1
6,43,307,0,350,bar,262,drager
7,45,308,0,350,bar,0,inter2
8,45,306,0,350,bar,0,inter2
9,44,309,0,350,bar,255,inter1
10,45,305,0,350,bar,152,inter2