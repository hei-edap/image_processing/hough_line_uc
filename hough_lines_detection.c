/*
 * Compile this file together with the SOD embedded source code to generate
 * the executable. For example:
 *
 *  gcc sod.c hough_lines_detection.c -lm -Ofast -march=native -Wall -std=c99 -o sod_img_proc
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "sod.h"

struct line_t {
    int a;
    int b;
    int c;
};

typedef struct line_t line;

struct line_point_t{
    sod_pts p1;
    sod_pts p2;
};

typedef struct line_point_t line_point;

// Percentage of the original image is considered during detection
static const double REDUCTION_FACTOR = 0.5;
static const int INITIAL_THRESHOLD = 90;

// Function to find the line given two points
line lineFromPoints(int x1, int y1, int x2, int y2) {
    line result;
    int a = y2 - y1;
    int b = x1 - x2;
    int c = (a * (x1) + b * (y1));

    if (b < 0)
        printf("Line is %d x %d y = %d ", a, b, c);
    else
        printf("Line is %d x + %d y = %d", a, b, c);

    printf("\n");
    result.a = a;
    result.b = b;
    result.c = c;
    return result;
}

bool intersection(sod_pts a, sod_pts b, sod_pts c, sod_pts d, sod_pts* result){
    float a1 = b.y - a.y;
    float b1 = a.x - b.x;
    float c1 = a1 * a.x + b1 * a.y;

    float a2 = d.y - c.y;
    float b2 = c.x - d.x;
    float c2 = a2 * c.x + b2 * c.y;

    float deter = a1 * b2 - a2 * b1;

    //double dist = sqrt(pow(inter.x - center.x, 2) + pow(inter.y - center.y, 2));
    if(deter == 0){
        return false;
    }
    else{
        //If lines are parallel, the result will be (NaN, NaN).
        float x = (b2 * c1 - b1 * c2) / deter;
        float y = (a1 * c2 - a2 * c1) / deter;

        result->x = x;
        result->y = y;
        return true;
    }
}

bool goesInTheMiddle(line l, int cx, int cy, int radius) {
    // Finding the distance of line from center.
    double dist = (abs(l.a * cx + l.b * cy - l.c))
                  / sqrt(l.a * l.a + l.b * l.b);

    // Checking if the distance is less than,
    // greater than or equal to radius.
    if (radius >= dist)
        return true;
    else
        return false;
}

int main(int argc, char *argv[]) {
    /* Input image (pass a path or use the test image shipped with the samples ZIP archive) */
    const char *zInput = argc > 1 ? argv[1] : "./input/gauge-1.jpg";

    /* Processed output image path */
    // const char *zOut = argc > 2 ? argv[2] : "./output/out_lines.png";
    const char *optimized_arg = argc > 2 ? argv[2] : 0;

    bool use_optimized_hough;

    if(optimized_arg != 0 && atoi(optimized_arg) == 1)
        use_optimized_hough = true;
    else
        use_optimized_hough = false;

    printf("Optimized Hough line detector : %d\n", use_optimized_hough);
    const char *zOut = "./output/out_lines.png";

    /* Processed output image path */
    const char *canny = "./output/canny.png";
    const char *scaled = "./output/scaled.png";
    const char *hild = "./output/hild.png";

    /* Load the input image in the grayscale colorspace */
    sod_img imgIn = sod_img_load_grayscale(zInput);

    // Cropping factor for image
    // Size of the subwindow
    int wx = imgIn.w * REDUCTION_FACTOR, wy = imgIn.h * REDUCTION_FACTOR;

    // Position of the center
    int cx = imgIn.w / 2 - wx / 2, cy = imgIn.h / 2 -wy / 2;
    int radius = imgIn.w/20;

    // Crop image
    imgIn = sod_crop_image(imgIn, cx, cy, wx, wy);
    sod_img_save_as_png(imgIn, scaled);

    if (imgIn.data == 0) {
        /* Invalid path, unsupported format, memory failure, etc. */
        puts("Cannot load input image...exiting");
        return 0;
    }

    /* A full color copy of the input image so we can draw later rose lines on it.
     */
    sod_img imgCopy = sod_img_load_color(zInput);

    /*
     * Perform Canny edge detection first which is a mandatory step */
    sod_img cannyImg = sod_canny_edge_image(imgIn, 1);
    sod_img_save_as_png(cannyImg, canny);

    /*
     * Each detected line is represented by an instance of the `sod_pts`
     * structure returned as an array by the Hough interface where
     * each entry of this array (i and i + 1) hold the starting and
     * ending position (x_start, y_start, x_end, y_end) for each line.
     */
    sod_pts *aLines;
    int i, nPts, nLines;

    /* Perform hough line detection on the canny edged image
     * Depending on the analyzed image/frame, you should experiment
     * with different thresholds for best results.
     */
    int thresh = INITIAL_THRESHOLD;

    initCosTables();

    if(use_optimized_hough)
        aLines = sod_hough_lines_detect_optimized(cannyImg, thresh, &nPts);
    else
        aLines = sod_hough_lines_detect(cannyImg, thresh, &nPts);

    nLines = nPts / 2;

    while (nLines < 2) {
        printf("No lines detected, adapting threshold to %d\n", thresh);
        sod_hough_lines_release(aLines);
        thresh -= 10;

        if (thresh <= 0) {
            printf("No lines detected, quitting");
            exit(-1);
        }

        aLines = sod_hough_lines_detect(cannyImg, thresh, &nPts);
        nLines = nPts / 2;
    }

    printf("%d line(s) were detected\n", nLines);

    // Take the two closest to the center, intersect them. This intersection point
    // should be the point we are looking for. Compute the angle of this segment
    // from center and we have it.

    // The center of the cropped screen, in the big image coordinates
    int middle_x = cx + wx/2 ;
    int middle_y = cy + wy/2;

    int idx = 0;
    line_point linesCenter[10];

    // Draws the circle considered for keeping lines
    sod_image_draw_circle(imgCopy, middle_x, middle_y, radius, 0, 0, 255);

    /* Draw a rose line for each entry on the full color image copy */
    for (i = 0; i < nPts; i += 2) {
        line l = lineFromPoints(
                aLines[i].x + cx, aLines[i].y +cy,
                aLines[i + 1].x + cx, aLines[i + 1].y + cy);

        sod_pts p1, p2;
        p1.x = aLines[i].x + cx;
        p1.y = aLines[i].y + cy;
        p2.x = aLines[i+1].x + cx;
        p2.y = aLines[i+1].y + cy;

        // If the current line is close enough to center, keep it
        if (goesInTheMiddle(l, middle_x, middle_y, radius)) {
            line_point lp;
            lp.p1 = p1;
            lp.p2 = p2;
            linesCenter[idx] = lp;
            idx++;
            sod_image_draw_line(imgCopy, p1, p2, 0, 255, 0);
        } else {
            // sod_image_draw_line(imgCopy, p1, p2, 255, 0, 255);
        }
    }

    sod_pts inter;

    /**
     * TODO We should compute every possible intersection between the candidates lines, and keep only the farthest
     * intersection from the center
     */

    if(idx >= 2){
        sod_pts inter;
        bool intersect = intersection(linesCenter[0].p1, linesCenter[0].p2, linesCenter[1].p1, linesCenter[1].p2, &inter);

        if(intersect){
            sod_pts mid_sod;
            mid_sod.x = middle_x;
            mid_sod.y = middle_y;
            printf("Lines intersect at position %d, %d\n", inter.x, inter.y);
            sod_image_draw_circle(imgCopy, inter.x, inter.y, 5, 0, 255, 255);
            sod_image_draw_circle(imgCopy, mid_sod.x, mid_sod.y, 5, 0, 255, 255);
            sod_image_draw_line(imgCopy, inter, mid_sod, 255, 0, 0);
        }
    }
    else{
        printf("Single line, no intersection!");
    }

    /* Finally save the output image to the specified path */
    sod_img_save_as_png(imgCopy, zOut);
    /* Cleanup */
    sod_hough_lines_release(aLines);
    sod_free_image(imgIn);
    sod_free_image(cannyImg);
    sod_free_image(imgCopy);
    return 0;
}
